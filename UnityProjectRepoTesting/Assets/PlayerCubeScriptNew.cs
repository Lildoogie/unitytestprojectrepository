﻿using UnityEngine;
using System.Collections;

public class PlayerCubeScriptNew : MonoBehaviour {
    private float turnspeed = 5.0f;
    private float speed = 5.0f;
    private float trueSpeed = 0.0f;
    private float strafeSpeed = 5.0f;

    void Update()
    {

        var roll = Input.GetAxis("Roll");
        var pitch = Input.GetAxis("Pitch");
        var yaw = Input.GetAxis("Yaw");
        var strafe = new Vector3(Input.GetAxis("Horizontal") * strafeSpeed * Time.deltaTime, Input.GetAxis("Vertical") * strafeSpeed * Time.deltaTime, 0);

        var power = Input.GetAxis("Power");

        //Truespeed controls

        if (trueSpeed < 10 && trueSpeed > -3)
        {
            trueSpeed += power;
        }
        if (trueSpeed > 10)
        {
            trueSpeed = 9.99f;
        }
        if (trueSpeed < -3)
        {
            trueSpeed = -2.99f;
        }
        if (Input.GetKey("backspace"))
        {
            trueSpeed = 0;
        }

        this.GetComponent<Rigidbody>().AddRelativeTorque(pitch * turnspeed * Time.deltaTime, yaw * turnspeed * Time.deltaTime, roll * turnspeed * Time.deltaTime);
        this.GetComponent<Rigidbody>().AddRelativeForce(0, 0, trueSpeed * speed * Time.deltaTime);
        this.GetComponent<Rigidbody>().AddRelativeForce(strafe);
    }
}
